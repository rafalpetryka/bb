<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>Faktura - Panel zarządzania - Barn Burger</title>
    <meta name="author" content="IdeasFactory.pl Rafał Petryka">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../css/reset.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="shortcut icon" href="../img/favicon.ico">
</head>
<body>
    <a href="http://barnburger.pl"><div class="logo"><img src="../img/logo.png" alt="logo"></div></a>
    <div class="napis">Wypisz fakturę</div>
    <form action="faktura.php" method="post" id="faktura">
    <table>
						<br>
						<a href="dane.php" class="lista">Zobacz listę fakturobiorców</a>
						<br><br>
						<tr>
							<td><label>Numer faktury (ostatnio wprowadzony numer to <b>
							<?php
								$dane = fread(fopen("numer.txt", "r"), filesize("numer.txt"));
								echo $dane
							?>
							</b>):</label></td>
							<td><input type="text" size="4" name="id" maxlength="4" /></td>
						</tr>
						<tr>
							<td><br><label><b>Dane nabywcy:</b></label></td>
							<td></td>
						</tr>
						<tr>
							<td><label>Nazwa firmy:</label></td>
							<td><input type="text" size="70" name="nazwa_firmy" maxlength="50" /></td>
						</tr>
						<tr>
							<td><label>Ulica:</label></td>
							<td><input type="text" size="50" name="ulica" maxlength="30" /></td>
						</tr>
						<tr>
							<td><label>Numer (mieszkania):</label></td>
							<td><input type="text" size="15" name="numer_mieszkania" maxlength="10" /></td>
						</tr>
						<tr>
							<td><label>Kod pocztowy (z myślnikiem):</label></td>
							<td><input type="text" size="7" name="kod_pocztowy" maxlength="6" /></td>
						</tr>
						<tr>
							<td><label>Miejscowość:</label></td>
							<td><input type="text" size="58" name="miejscowosc" maxlength="35" /></td>
						</tr>
						<tr>
							<td><label>NIP:</label></td>
							<td><input type="text" size="10" name="nip" maxlength="10" /></td>
						</tr>
						<tr>
							<td><br><label><b>Zamówienie:</b></label></td>
							<td></td>
						</tr>
						<tr>
							<td><label>Data złożenia zamówienia: </label></td>
							<td>
								<select name="dzien_zamowienia">
									<option value = "<?php echo date("j"); ?>"><?php echo date("j"); ?>
									<option value = "1">01
									<option value = "2">02
									<option value = "3">03
									<option value = "4">04
									<option value = "5">05
									<option value = "6">06
									<option value = "7">07
									<option value = "8">08
									<option value = "9">09
									<option value = "10">10
									<option value = "11">11
									<option value = "12">12
									<option value = "13">13
									<option value = "14">14
									<option value = "15">15
									<option value = "16">16
									<option value = "17">17
									<option value = "18">18
									<option value = "19">19
									<option value = "20">20
									<option value = "21">21
									<option value = "22">22
									<option value = "23">23
									<option value = "24">24
									<option value = "25">25
									<option value = "26">26
									<option value = "27">27
									<option value = "28">28
									<option value = "29">29
									<option value = "30">30
									<option value = "31">31
								</select>
								<?php
									if (date("F")=="January")
									$miesiac="01";
									if (date("F")=="February")
									$miesiac="02";
									if (date("F")=="March")
									$miesiac="03";
									if (date("F")=="April")
									$miesiac="04";
									if (date("F")=="May")
									$miesiac="05";
									if (date("F")=="June")
									$miesiac="06";
									if (date("F")=="July")
									$miesiac="07";
									if (date("F")=="August")
									$miesiac="08";
									if (date("F")=="September")
									$miesiac="09";
									if (date("F")=="October")
									$miesiac="10";
									if (date("F")=="November")
									$miesiac="11";
									if (date("F")=="December")
									$miesiac="12";
									?>
								<select name="miesiac_zamowienia">
									<option value = "<?php echo $miesiac; ?>"><?php echo $miesiac; ?>
									<option value = "01">01
									<option value = "02">02
									<option value = "03">03
									<option value = "04">04
									<option value = "05">05
									<option value = "06">06
									<option value = "07">07
									<option value = "08">08
									<option value = "09">09
									<option value = "10">10
									<option value = "11">11
									<option value = "12">12
								</select>
								<select name="rok_zamowienia">
									<option value = "<?php echo date("Y"); ?>"><?php echo date("Y"); ?>	
									<option value = "2013">2013
									<option value = "2014">2014
									<option value = "2015">2015
								</select>
							</td>
						</tr>
						<tr>
							<td><label>Data wystawienia: </label></td>
							<td>
								<select name="dzien_wystawienia">
									<option value = "<?php echo date("j"); ?>"><?php echo date("j"); ?>
									<option value = "1">01
									<option value = "2">02
									<option value = "3">03
									<option value = "4">04
									<option value = "5">05
									<option value = "6">06
									<option value = "7">07
									<option value = "8">08
									<option value = "9">09
									<option value = "10">10
									<option value = "11">11
									<option value = "12">12
									<option value = "13">13
									<option value = "14">14
									<option value = "15">15
									<option value = "16">16
									<option value = "17">17
									<option value = "18">18
									<option value = "19">19
									<option value = "20">20
									<option value = "21">21
									<option value = "22">22
									<option value = "23">23
									<option value = "24">24
									<option value = "25">25
									<option value = "26">26
									<option value = "27">27
									<option value = "28">28
									<option value = "29">29
									<option value = "30">30
									<option value = "31">31
								</select>
								<select name="miesiac_wystawienia">
									<option value = "<?php echo $miesiac; ?>"><?php echo $miesiac; ?>
									<option value = "01">01
									<option value = "02">02
									<option value = "03">03
									<option value = "04">04
									<option value = "05">05
									<option value = "06">06
									<option value = "07">07
									<option value = "08">08
									<option value = "09">09
									<option value = "10">10
									<option value = "11">11
									<option value = "12">12
								</select>
								<select name="rok_wystawienia">
									<option value = "<?php echo date("Y"); ?>"><?php echo date("Y"); ?>
									<option value = "2013">2013
									<option value = "2014">2014
									<option value = "2015">2015
								</select>
							</td>
						</tr>
						<tr>
							<td><label>Forma płatności: </label></td>
							<td>
								<select name="platnosc">
									<option value = "">
									<option value = "Karta płatnicza">Karta płatnicza
									<option value = "Gotówka">Gotówka
								</select>
							</td>
						</tr>
						<tr>
							<td><label>Nazwa produktu / ilość / vat (%)/ cena brutto (za sztukę): </label></td>
							<td><input type="text" size="35" value="Za konsumpcję" name="nazwa1" maxlength="25" /><input type="text" size="1" name="ilosc1" value="1" maxlength="3" /><input type="text" size="1" name="vat1" value="5" maxlength="2" /><input type="text" size="3" name="cena1" maxlength="6" /></td>
						</tr>
						<tr>
							<td><label>Nazwa produktu / ilość / vat (%)/ cena brutto (za sztukę): </label></td>
							<td><input type="text" size="35" value=" " name="nazwa2" maxlength="25" /><input type="text" size="1" name="ilosc2" value="1" maxlength="3" /><input type="text" size="1" name="vat2" value="8" maxlength="2" /><input type="text" size="3" name="cena2" maxlength="6" /></td>
						</tr>
						<tr>
							<td><label>Nazwa produktu / ilość / vat (%)/ cena brutto (za sztukę): </label></td>
							<td><input type="text" size="35" value="" name="nazwa3" maxlength="25" /><input type="text" size="1" name="ilosc3" value="1" maxlength="3" /><input type="text" size="1" name="vat3" value="23" maxlength="2" /><input type="text" size="3" name="cena3" maxlength="6" /></td>
						</tr>
						<tr>
							<td><label>Nazwa produktu / ilość / vat (%)/ cena brutto (za sztukę): </label></td>
							<td><input type="text" size="35" value="" name="nazwa4" maxlength="25" /><input type="text" size="1" name="ilosc4" value="" maxlength="3" /><input type="text" size="1" name="vat4" value="" maxlength="2" /><input type="text" size="3" name="cena4" maxlength="6" /></td>
						</tr>
						<tr>
							<td><label>Nazwa produktu / ilość / vat (%)/ cena brutto (za sztukę): </label></td>
							<td><input type="text" size="35" value="" name="nazwa5" maxlength="25" /><input type="text" size="1" name="ilosc5" value="" maxlength="3" /><input type="text" size="1" name="vat5" value="" maxlength="2" /><input type="text" size="3" name="cena5" maxlength="6" /></td>
						</tr>
						<tr>
							<td><label>Nazwa produktu / ilość / vat (%)/ cena brutto (za sztukę): </label></td>
							<td><input type="text" size="35" value="" name="nazwa6" maxlength="25" /><input type="text" size="1" name="ilosc6" value="" maxlength="3" /><input type="text" size="1" name="vat6" value="" maxlength="2" /><input type="text" size="3" name="cena6" maxlength="6" /></td>
						</tr>
						<tr>
							<td><label>Nazwa produktu / ilość / vat (%)/ cena brutto (za sztukę): </label></td>
							<td><input type="text" size="35" value="" name="nazwa7" maxlength="25" /><input type="text" size="1" name="ilosc7" value="" maxlength="3" /><input type="text" size="1" name="vat7" value="" maxlength="2" /><input type="text" size="3" name="cena7" maxlength="6" /></td>
						</tr>
						<tr>
							<td><label>Nazwa produktu / ilość / vat (%)/ cena brutto (za sztukę): </label></td>
							<td><input type="text" size="35" value="" name="nazwa8" maxlength="25" /><input type="text" size="1" name="ilosc8" value="" maxlength="3" /><input type="text" size="1" name="vat8" value="" maxlength="2" /><input type="text" size="3" name="cena8" maxlength="6" /></td>
						</tr>
						<tr>
							<td><label>Nazwa produktu / ilość / vat (%)/ cena brutto (za sztukę): </label></td>
							<td><input type="text" size="35" value="" name="nazwa9" maxlength="25" /><input type="text" size="1" name="ilosc9" value="" maxlength="3" /><input type="text" size="1" name="vat9" value="" maxlength="2" /><input type="text" size="3" name="cena9" maxlength="6" /></td>
						</tr>
						<tr>
							<td><label>Nazwa produktu / ilość / vat (%)/ cena brutto (za sztukę): </label></td>
							<td><input type="text" size="35" value="" name="nazwa10" maxlength="25" /><input type="text" size="1" name="ilosc10" value="" maxlength="3" /><input type="text" size="1" name="vat10" value="" maxlength="2" /><input type="text" size="3" name="cena10" maxlength="6" /></td>
						</tr>
						<td colspan="2" align="center"><br><input type="submit" value="Generuj fakturę" class="button"></td>
					</table>
					</form>
    <a href="http://ideasfactory.pl" target="_blank" id="ideasfactory">© IdeasFactory.pl</a>
</body>
</html>