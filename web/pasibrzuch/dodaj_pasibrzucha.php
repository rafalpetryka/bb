<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>Dodaj Pasibrzucha - Panel zarządzania - Barn Burger</title>
    <meta name="author" content="IdeasFactory.pl Rafał Petryka">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="shortcut icon" href="img/favicon.ico">
</head>
<body>
    <a href="http://barnburger.pl"><div class="logo"><img src="../img/logo.png" alt="logo"></div></a>
    <div class="napis">
        Dodaj pasibrzucha
        <span>
        Jeżeli uczestnik się poddał to wpisujemy tylko jego imię i nazwisko :)</span>
    </div>
    <div class="form">
        <form action="dodano_pasibrzucha.php" method="post">
            Imię i Nazwisko:<br>
            <input type="text" class="imie" name="imie" maxlength="60"><br>
            Minuty<br>
            <input type="text" name="minuty" maxlength="2"><br>
            Sekundy<br>
            <input type="text" name="sekundy" maxlength="2"><br>
            Setne sekundy<br>
            <input type="text" name="setnesekundy" maxlength="2"><br>
            <input type="submit" value="Dodaj pasibrzucha">
        </form>
    </div>
    <a href="http://ideasfactory.pl" target="_blank" id="ideasfactory">© IdeasFactory.pl</a>
</body>
</html>